var jsapp = new Vue ({
    el: '#js-app',
    data: {
        color: '#' + (Math.random()*0xFFFFFF<<0).toString(16)
    },
    methods: {
        generator: function(event) {
            this.color = '#' + (Math.random()*0xFFFFFF<<0).toString(16);
            document.getElementById(event.currentTarget.id).style.background = this.color;
            document.getElementById(event.currentTarget.id).innerHTML = this.color;
        },
        all: function() {
            var classes = document.getElementsByClassName("col");
            console.log(classes.length);
            for (let index = 0; index < classes.length; index++) {
                const element = classes[index].id;
                this.color = '#' + (Math.random()*0xFFFFFF<<0).toString(16);
                document.getElementById(element).style.background = this.color;
                document.getElementById(element).innerHTML = this.color;
            }
        }
    }
});
